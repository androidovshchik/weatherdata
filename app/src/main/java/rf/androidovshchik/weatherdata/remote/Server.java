package rf.androidovshchik.weatherdata.remote;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rf.androidovshchik.weatherdata.models.WeatherData;

public interface Server {

    @GET("getWeatherData.php")
    Observable<WeatherData[]> getWeatherData(@Query("drone_id") long droneId,
                                           @Query("drone_pass") long dronePassword,
                                           @Query("probe_time") long probeTime);
}
