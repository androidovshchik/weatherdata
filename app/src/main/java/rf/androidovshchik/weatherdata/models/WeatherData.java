package rf.androidovshchik.weatherdata.models;

import com.google.gson.annotations.SerializedName;

public class WeatherData {

    @SerializedName("drone_id")
    public long droneId;

    @SerializedName("drone_pass")
    public long dronePassword;

    @SerializedName("probe_id")
    public long probeId;

    @SerializedName("probe_time")
    public long probeTime;

    @SerializedName("probe_lat")
    public long probeLat;

    @SerializedName("probe_lon")
    public long probeLon;

    @SerializedName("probe_hum")
    public long probeHum;

    @SerializedName("probe_temp")
    public long probeTemp;

    @SerializedName("probe_press")
    public long probePress;

    @Override
    public String toString() {
        return "WeatherData{" +
            "droneId=" + droneId +
            ", dronePassword=" + dronePassword +
            ", probeId=" + probeId +
            ", probeTime=" + probeTime +
            ", probeLat=" + probeLat +
            ", probeLon=" + probeLon +
            ", probeHum=" + probeHum +
            ", probeTemp=" + probeTemp +
            ", probePress=" + probePress +
            '}';
    }
}
