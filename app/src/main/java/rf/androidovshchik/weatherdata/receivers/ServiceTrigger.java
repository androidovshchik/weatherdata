package rf.androidovshchik.weatherdata.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import rf.androidovshchik.weatherdata.services.MainService;
import rf.androidovshchik.weatherdata.utils.ServiceUtil;
import timber.log.Timber;

public class ServiceTrigger extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("ServiceTrigger: received");
        ServiceUtil.startServiceRightWay(context, MainService.class);
	}
}
