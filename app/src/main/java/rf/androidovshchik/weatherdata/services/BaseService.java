package rf.androidovshchik.weatherdata.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;

import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.weatherdata.R;
import rf.androidovshchik.weatherdata.SettingsActivity;
import rf.androidovshchik.weatherdata.data.Preferences;
import rf.androidovshchik.weatherdata.receivers.ToastTrigger;
import rf.androidovshchik.weatherdata.utils.AlarmUtil;

public abstract class BaseService extends Service {

	private PowerManager.WakeLock wakeLock;

	protected CompositeDisposable disposable = new CompositeDisposable();

	protected Preferences preferences;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	@SuppressWarnings("all")
	public void onCreate() {
		super.onCreate();
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
			getString(R.string.app_name));
		wakeLock.acquire();
		preferences = new Preferences(getApplicationContext());
		AlarmUtil.next(getApplicationContext());
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_NOT_STICKY;
	}

	@SuppressWarnings("all")
	protected void startForeground(int id, String title, @DrawableRes int image) {
		NotificationManager notificationManager = (NotificationManager)
			getSystemService(Context.NOTIFICATION_SERVICE);
		Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
			getString(R.string.app_name))
			.setSmallIcon(image)
			.setContentTitle(title)
			.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT))
			.setSound(null);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(getString(R.string.app_name),
				getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);
			channel.setSound(null, null);
			notificationManager.createNotificationChannel(channel);
			builder.setChannelId(getString(R.string.app_name));
		}
		startForeground(id, builder.build());
	}

	protected void stopWork() {
		stopForeground(true);
		stopSelf();
	}

	public void showMessage(String message) {
		Intent intent = new Intent();
		intent.setAction(getPackageName() + ".TOAST");
		intent.putExtra(ToastTrigger.EXTRA_MESSAGE, message);
		sendBroadcast(intent);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		disposable.dispose();
		wakeLock.release();
	}
}
