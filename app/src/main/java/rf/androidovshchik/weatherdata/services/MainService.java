package rf.androidovshchik.weatherdata.services;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.gson.GsonBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rf.androidovshchik.weatherdata.BuildConfig;
import rf.androidovshchik.weatherdata.R;
import rf.androidovshchik.weatherdata.data.Preferences;
import rf.androidovshchik.weatherdata.models.WeatherData;
import rf.androidovshchik.weatherdata.remote.Server;
import timber.log.Timber;

public class MainService extends BaseService {

	private WindowManager windowManager;

	private View floatyView;
	private TextView temp;
	private TextView hum;
	private TextView press;

	private Server api;

	private CompositeDisposable requestDisposable = new CompositeDisposable();

	@Override
	@SuppressWarnings("all")
	public void onCreate() {
		super.onCreate();
		windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		if (BuildConfig.DEBUG) {
			HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
			logging.setLevel(HttpLoggingInterceptor.Level.BODY);
			httpClient.addInterceptor(logging);
		}
		Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(validUrl(preferences.getString(Preferences.SERVER_URL)))
			.addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
				.create()))
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.client(httpClient.build())
			.build();
		api = retrofit.create(Server.class);
		startForeground(1, "Взаимодействие с сервером", R.drawable.ic_cloud_white_24dp);
	}

	@Override
	@SuppressWarnings("all")
	public int onStartCommand(Intent intent, int flags, int startId) {
		Timber.d("onStartCommand: Main service");
        addOverlayView();
		disposable.add(Observable.interval(0, 5, TimeUnit.SECONDS)
			.subscribeOn(Schedulers.io())
			.subscribe((Long value) -> {
				long id, password, time = preferences.getLong(Preferences.LAST_TIME, 0);
				try {
					id = Long.parseLong(preferences.getString(Preferences.DRONE_ID));
					password = Long.parseLong(preferences.getString(Preferences.DRONE_PASSWORD));
				} catch (NumberFormatException e) {
					Timber.e(e);
					id = password = 0L;
				}
				requestDisposable.clear();
				requestDisposable.add(api.getWeatherData(id, password, time)
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(this::handleResponse, this::handleError));
			}));
		return super.onStartCommand(intent, flags, startId);
	}

	private void handleResponse(WeatherData[] data) {
		if (data.length > 0) {
			WeatherData max = Collections.max(Arrays.asList(data), (WeatherData a, WeatherData b) ->
				Long.compare(a.probeTime, b.probeTime));
			preferences.putLong(Preferences.LAST_TIME, max.probeTime);
			temp.setText(String.valueOf(max.probeTemp));
			hum.setText(String.valueOf(max.probeHum));
			press.setText(String.valueOf(max.probePress));
		}
	}

	private void handleError(Throwable throwable) {
		Timber.e(throwable);
	}

	private void addOverlayView() {
		if (!Settings.canDrawOverlays(getApplicationContext())) {
			showMessage("Нет разрешения для показа поверх других окон");
			return;
		}
		floatyView = View.inflate(getApplicationContext(), R.layout.floating_view, null);
		try {
			floatyView.setBackgroundColor(Color.parseColor(preferences
				.getString(Preferences.BACKGROUND_COLOR)));
		} catch (IllegalArgumentException e) {
			Timber.e(e);
			temp.setTextColor(Color.WHITE);
		}
		temp = floatyView.findViewById(R.id.temp);
		hum = floatyView.findViewById(R.id.hum);
		press = floatyView.findViewById(R.id.press);
		try {
			int textColor = Color.parseColor(preferences.getString(Preferences.FONT_COLOR));
			temp.setTextColor(textColor);
			hum.setTextColor(textColor);
			press.setTextColor(textColor);
		} catch (IllegalArgumentException e) {
			Timber.e(e);
			temp.setTextColor(Color.BLACK);
			hum.setTextColor(Color.BLACK);
			press.setTextColor(Color.BLACK);
		}
		try {
			int textSize = Integer.parseInt(preferences.getString(Preferences.FONT_SIZE));
			temp.setTextSize(textSize);
			hum.setTextSize(textSize);
			press.setTextSize(textSize);
		} catch (NumberFormatException e) {
			Timber.e(e);
			temp.setTextSize(16);
			hum.setTextSize(16);
			press.setTextSize(16);
		}
		temp.setText(String.valueOf("0"));
		hum.setText(String.valueOf("0"));
		press.setText(String.valueOf("0"));
		WindowManager.LayoutParams params = new WindowManager.LayoutParams();
		params.height = WindowManager.LayoutParams.WRAP_CONTENT;
		params.width = WindowManager.LayoutParams.WRAP_CONTENT;
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
		} else {
			params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
		}
		params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
			| WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
			| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
			| WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS;
		params.format = PixelFormat.TRANSLUCENT;
		params.gravity = Gravity.TOP | Gravity.START;
		try {
			params.x = Integer.parseInt(preferences.getString(Preferences.X));
			params.y = Integer.parseInt(preferences.getString(Preferences.Y));
		} catch (NumberFormatException e) {
			Timber.e(e);
			params.x = params.y = 0;
		}
		windowManager.addView(floatyView, params);
	}

	private String validUrl(String url) {
		if (url.length() > 0) {
			if (url.charAt(url.length() - 1) != '/') {
				url += "/";
			}
		} else {
			url = "http://www.mchsserv.ru/api/";
		}
		return url;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (floatyView != null) {
			windowManager.removeView(floatyView);
		}
	}
}
