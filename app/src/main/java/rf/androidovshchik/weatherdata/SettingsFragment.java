package rf.androidovshchik.weatherdata;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import rf.androidovshchik.weatherdata.data.Preferences;
import rf.androidovshchik.weatherdata.services.MainService;
import rf.androidovshchik.weatherdata.utils.ServiceUtil;
import timber.log.Timber;

public class SettingsFragment extends PreferenceFragment {

    private Preferences preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        preferences = new Preferences(getContext());
        if (preferences.getBoolean(Preferences.SHOW_WINDOW) &&
            !ServiceUtil.isRunning(getContext(), MainService.class)) {
            ServiceUtil.startServiceRightWay(getContext(), MainService.class);
        }
        findPreference(Preferences.SHOW_WINDOW)
            .setOnPreferenceChangeListener((Preference preference, Object newValue) -> {
                Timber.d(preference.getKey() + ": " + newValue);
                if ((Boolean) newValue) {
                    ServiceUtil.startServiceRightWay(getContext(), MainService.class);
                } else {
                    ServiceUtil.stopServiceRightWay(getContext(), MainService.class);
                }
                return true;
            });
        findPreference(Preferences.DRONE_ID)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
        findPreference(Preferences.DRONE_PASSWORD)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
        findPreference(Preferences.SERVER_URL)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
        findPreference(Preferences.FONT_SIZE)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
        findPreference(Preferences.FONT_COLOR)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
        findPreference(Preferences.BACKGROUND_COLOR)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
        findPreference(Preferences.X)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
        findPreference(Preferences.Y)
            .setOnPreferenceChangeListener(this::onPreferenceChange);
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Timber.d(preference.getKey() + ": " + newValue);
        if (preference.getKey().equals(Preferences.DRONE_ID) ||
            preference.getKey().equals(Preferences.DRONE_PASSWORD) ||
            preference.getKey().equals(Preferences.SERVER_URL)) {
            preferences.remove(Preferences.LAST_TIME);
        }
        if (ServiceUtil.isRunning(getContext(), MainService.class)) {
            ServiceUtil.startServiceRightWay(getContext(), MainService.class);
        }
        return true;
    }
}