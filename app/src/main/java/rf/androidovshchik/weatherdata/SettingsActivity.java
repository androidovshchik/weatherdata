package rf.androidovshchik.weatherdata;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;

public class SettingsActivity extends AppCompatPreferenceActivity {

    public final static int REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Настройки");
        SettingsFragment settings = new SettingsFragment();
        getFragmentManager().beginTransaction()
            .replace(android.R.id.content, settings)
            .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!Settings.canDrawOverlays(getApplicationContext())) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                showMessage("Требуется разрешение для показа поверх других окон");
            }
        }
    }

    public void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
            message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
}
