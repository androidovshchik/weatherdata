<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: application/json; charset=utf-8');

require __DIR__ . "/../vendor/autoload.php";

use Medoo\Medoo;

function get(&$var, $default = null) {
    return isset($var) ? $var : $default;
}

if (empty($_GET)) {
    exit;
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../config.json"), true));

echo json_encode($db->select("mchserv", "*", [
	"AND" => [
	    "drone_id" => get($_GET["drone_id"], 0),
	    "drone_pass" => get($_GET["drone_pass"], 0),
	    "probe_time[>]" => get($_GET["probe_time"], 0)
	]
]), JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);