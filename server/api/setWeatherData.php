<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: GET');
header('Content-Type: text/html; charset=utf-8');

require __DIR__ . "/../vendor/autoload.php";

use Medoo\Medoo;

function get(&$var, $default = null) {
    return isset($var) ? $var : $default;
}

if (empty($_GET)) {
    exit;
}

$db = new Medoo(json_decode(file_get_contents(__DIR__ . "/../config.json"), true));

$db->insert("mchserv", [
    "drone_id" => get($_GET["drone_id"], 0),
    "drone_pass" => get($_GET["drone_pass"], 0),
    "probe_time" => get($_GET["probe_time"], 0),
    "probe_lat" => get($_GET["probe_lat"], 0),
    "probe_lon" => get($_GET["probe_lon"], 0),
    "probe_hum" => get($_GET["probe_hum"], 0),
    "probe_temp" => get($_GET["probe_temp"], 0),
    "probe_press" => get($_GET["probe_press"], 0)
]);

echo $db->id();
